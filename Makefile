CC=cc
CFLAGS=-O1
PREFIX=/usr

everything: wurl .ok

.ok: wurl
	./test.sh ./wurl
	touch .ok

wurl: wurl.c
	$(CC) $(CFLAGS) -o wurl wurl.c

wurl.c: wurl.scm
	# no need to optimize something so trivial
	ol -o wurl.c wurl.scm

install: wurl
	mkdir -p $(PREFIX)/bin
	install -m 755 wurl $(PREFIX)/bin/wurl

uninstall:
	-rm $(PREFIX)/bin/wurl
