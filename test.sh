#!/bin/sh

fail() {
   echo "FAIL: $@"
   exit 1
}

$@ slartibartfast | grep -q slartibartfast || fail "no ASCII input seen in output"

A=$($@ foo bar)
B=$($@ "foo bar")
C=$(echo foo bar | $@ 2>/dev/null)

test "$A" = "$B" || fail "outputs differ 1: $A $B"
test "$A" = "$C" || fail "outputs differ 2: $A $C"
test "$B" = "$C" || fail "your computer is drunk"

echo "$@ passed sanity tests"
