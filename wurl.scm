#!/usr/bin/ol -r

;; wurl - construct an url from input

(import (owl args))

; todo:
;  - add different search engines
;  - $ www --engine google --search foo bar
;  - $ www prisma aukioloajat -> google i'm feeling lucky
;  - $ www --engine wolframalpha 
;  - $ www --config $HOME/.wwwrc -r sää oulu -> find custom rule sää, insert url-encoded oulu to %s
;  - $ www --browser elinks --engine google
;    search: where does santa live?
;    ^ if no arguments, read first line from stdin

(define (first-line)
   (lets
      ((input (lines stdin))
       (first input (uncons input #false)))
      (or first "")))

;; (pattern name ...)
(define search-engines
   '(("http://www.google.com/search?q=%s" "google" "g")
     ("http://www.wolframalpha.com/input/?i=%s" "wolframalpha" "wa")
     ("http://duckduckgo.com/?q=%s&kp=-1&kl=us-en" "duckduckgo" "ddg")
     ("http://duckduckgo.com/lite?q=%s" "duckduckgolite" "ddgl")
     ("http://dictionary.reference.com/dic?q=%s&s=t" "dictionary" "dict")))

(define default-engine "duckduckgolite")

(define (engine-pattern name)
   (let loop ((es search-engines))
      (cond
         ((null? es) #false)
         ((mem equal? (cdar es) name) (caar es))
         (else (loop (cdr es))))))

(define command-line-rules
   (cl-rules
      `((engine "-e" "--engine" has-arg comment "choose search engine" 
           default ,default-engine)
        (list "-l" "--list" comment "list known engines")
        (help "-h" "--help" comment "show this thing")
        (about "-a" "--about" comment "what is this thing?")
        (verbose "-v" "--verbose" comment "show progress during generation"))))

;; very strict url encoder
;; str → (byte ...)
(define (encode->list str)
   (foldr 
      (λ (char tl)
         (cond
            ((eq? char #\space)
               (cons #\+ tl))
            ((m/[a-zA-Z0-9]/ (list char))
               (cons char tl))
            (else
               (cons #\%
                  (append
                     (cdr
                        (string->list
                           (number->string (+ char 256) 16)))
                     tl)))))
      null (string->bytes str)))

(define (fill-pattern pat input-list)
   (list->string
      (foldr
         (λ (char tl)
            (if (eq? char #\%)
               (case (if (null? tl) #false (car tl))
                  ((#\s)
                     (append input-list (cdr tl)))
                  (else
                     (cons char tl)))
               (cons char tl)))
         (list #\newline) ;; implicit newline
         (string->list pat)))) ;; allow the query to have UTF-8 elsewhere

(define (wurl-query dict str)
   (let ((pat (engine-pattern (getf dict 'engine))))
      (if pat
         (begin
            (display (fill-pattern pat (encode->list str)))
            0)
         (begin
            (print-to stderr "unknown engine. known ones are: " (foldr append null (map cdr search-engines)))
            1))))
   
(define usage-text "Usage: wurl [args] [string] ...")

(define about-text "wurl: construct URLs (e.g. search engine queries) based on command line arguments or standard input")

(define (start-wurl dict args)
   (cond
      ((getf dict 'help) 
         (and 
            (print usage-text)
            (print-rules command-line-rules)
            0))
      ((getf dict 'about)
         (and
            (print about-text)
            0))
      ((getf dict 'list)
         (and
            (fold (λ (ok? thing) (and ok? (print  thing))) #true 
               (cons "Engines: "
                  (map 
                     (λ (thing)
                        (foldr 
                           (λ (a b) (string-append a (string-append " " b)))
                           (string-append "-> " (car thing))
                           (cons "  " (cdr thing))))
                     search-engines)))
            0))
      ((null? args)
         ;; no arguments given, read one line from stdin
         (display-to stderr "wurl: ")
         (lets ((first rest (uncons (lines stdin) "")))
            (wurl-query dict first)))
      (else
         (wurl-query dict
            (foldr (λ (string tail) (if (equal? tail "") string (string-append string (string-append " " tail))))
               ""
               args)))))

(define (wurl args)
   (process-arguments (cdr args) 
      command-line-rules usage-text start-wurl))

wurl

